import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import mountainhuts.MountainHut;
import mountainhuts.Municipality;
import mountainhuts.Region;

public class TestClass {
	
	Region region = Region.fromFile("Piemonte", "./mountain_huts.csv");
	
	@Test
	public void testCreateOrGetMunicipality() {
		Municipality mun = region.createOrGetMunicipality("Paullo", "MI", 750);
		assertNotNull(mun);
		assertEquals(region.createOrGetMunicipality("Paullo","MI", 750), mun);
	}

	@Test
	public void testMountainHutAltitude() {
		MountainHut hut = region.createOrGetMountainHut("HutNoAltitude", "Rifugio", 30, region.createOrGetMunicipality("Paullo", "MI", 750));
		assertTrue(region.getMountainHuts().stream().filter(f -> f.getName() == "HutNoAltitude").findFirst().get().getAltitude().isEmpty());
		MountainHut hutWithAltitude = region.createOrGetMountainHut("HutWithAltitude", 750, "Rifugio", 30, region.createOrGetMunicipality("Paullo", "MI", 750));
		assertTrue(region.getMountainHuts().stream().filter(f -> f.getName() == "HutWithAltitude").findFirst().get().getAltitude().get() == 750);
	}

}
